const app = new Vue({
  el: "#app",
  data: {
    saludo: "Soy el ciclo de vida de Vue.js 2",
  },
  beforeCreate() {
    console.log("beforeCreate");
  },
  created() {
    console.log("created");
  },
  beforeMount() {
    console.log("beforeMount");
  },
});
